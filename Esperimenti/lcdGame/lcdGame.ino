/*

  The circuit:
 * LCD RS pin to digital pin 10
 * LCD Enable pin to digital pin 13
 * LCD D4 pin to digital pin 5
 * LCD D5 pin to digital pin 4
 * LCD D6 pin to digital pin 3
 * LCD D7 pin to digital pin 2
 * LCD R/W pin to ground
 * LCD VSS pin to ground
 * LCD VCC pin to 5V
 * LCD Anode 10K resistor -> +5v
 * LCD Katode to ground
 * LCD VO pin -> 3kOhm -> +5v

 */
#define _TASK_PRIORITY
#define _TASK_SLEEP_ON_IDLE_RUN
#define _TASK_WDT_IDS
#define _TASK_TIMECRITICAL
#include <LiquidCrystal.h>
#include "./TaskScheduler.h"

#define tonePin A5
#define moveLeftPin 2
#define moveRightPin 3
// initialize the library with the numbers of the interface pins
LiquidCrystal lcd(10, 13, 7, 6, 5, 4);

byte block_left[3][8] = {
  {
    B11111,
    B11111,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
  },
  {
    B11111,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000,
    B00000,
    B00000,
  },
  {
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000,
  }
};

byte block_right[3][8] = {
  {
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
  },
  {
    B00000,
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,

  },
  {
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
    B11111,
  }
};

int difficultTiming;

byte borderLeft[4] = {0,1,2,255}; //contains indexes of character mapped on lcd for left border.
byte borderRight[4] = {3,4,5,255}; //contains indexes of character mapped on lcd for right border.

byte mL[16]; //contains indexes of borders left Map.
byte mR[16];
byte playerPos = 7; //this has range from 0 to 15

void copy(char* src, char* dst, int len) {
    memcpy(dst, src, sizeof(src[0])*len);
}
Task nextMapStepTask(1000, TASK_FOREVER, []() {
  //lcd.scrollDisplayLeft();
  for(byte i = 1; i < 16; i++) {
    mL[i-1] = mL[i];
    mR[i-1] = mR[i];
  }

  if(mL[0] != 3) {
    byte playerInCharL[8];
    copy(block_left[mL[0]],playerInCharL,8);
    if(playerPos < 7) {
      playerInCharL[playerPos] = B11111;
      playerInCharL[playerPos+1] = B11111;
    } else if(playerPos == 7) {
      playerInCharL[playerPos] = B11111;
    }
    lcd.createChar(6, playerInCharL);
  }
  if(mR[0] != 3) {
    byte playerInCharR[8];
    copy(block_right[mR[0]],playerInCharR,8);
    if(playerPos > 7) {
      playerInCharR[playerPos-8] = B11111;
      playerInCharR[playerPos-7] = B11111;
    } else if(playerPos == 7) {
      playerInCharR[0] = B11111;
    }
    lcd.createChar(7, playerInCharR);
  }
  lcd.clear(); //fix bugged display for a strange reason.
  
  lcd.setCursor(0,0);
  lcd.write(6);
    
  lcd.setCursor(0,1); 
  lcd.write(7);

  
  boolean ok = false;
  while(!ok) {
    mL[15] = (mL[14]+random(-1,2))%4;
    mR[15] = (mR[14]+random(-1,2))%4;
    if(mL[15] + mR[15] < 5) ok = true; //step achievable
  }
  
  lcd.setCursor(1,0);
  for(byte i = 1; i < 16; i++) 
    lcd.write(borderLeft[mL[i]]);

  lcd.setCursor(1,1);
  for(byte i = 1; i < 16; i++) 
    lcd.write(borderRight[mR[i]]);

  if(isHit()) {
    lcd.clear();
    nextMapStepTask.disable();
    lcd.setCursor(0,0);
    lcd.print("Game     "); //Points
    lcd.setCursor(0,1);
    lcd.print("     Over   ");//+(millis()/(1000*difficultTiming)));
  } else {
    difficultTiming = analogRead(A0);
    difficultTiming = map(difficultTiming, 0, 1024, 0, 2000);
    nextMapStepTask.setInterval(difficultTiming);
  }
});


Scheduler r1;
void setup() {
  Serial.begin(115200);
  pinMode(moveLeftPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(moveLeftPin), movePlayerLeft, RISING);
  pinMode(moveRightPin, INPUT_PULLUP);
  attachInterrupt(digitalPinToInterrupt(moveRightPin), movePlayerRight, RISING);
  pinMode(A0,INPUT);
  // set up the LCD's number of columns and rows:
  lcd.createChar(0, block_left[0]);
  lcd.createChar(1, block_left[1]);
  lcd.createChar(2, block_left[2]);
  lcd.createChar(3, block_right[0]);
  lcd.createChar(4, block_right[1]);
  lcd.createChar(5, block_right[2]);
  lcd.begin(16, 2);
  // Print a message to the LCD.
  randomSeed(analogRead(0));
  initGame();
  r1.init();
  r1.addTask(nextMapStepTask);
  nextMapStepTask.enable();
}

void loop() {
  r1.execute();
}

void initGame() {
  //Spawn Player
  byte playerInCharL[8] = {
    B11111,
    B11111,
    B11111,
    B11111,
    B00000,
    B00000,
    B00000,
    B11111,
  };
  byte playerInCharR[8] = {
    B11111,
    B00000,
    B00000,
    B00000,
    B11111,
    B11111,
    B11111,
    B11111,
  };
  lcd.createChar(6, playerInCharL);
  lcd.createChar(7, playerInCharR);
  lcd.clear(); //fix bugged display for a strange reason.
  
  lcd.setCursor(0,0);
  lcd.write(6);
    
  lcd.setCursor(0,1); 
  lcd.write(7);
  
  //Generate Map
  mL[0] = 1;
  mR[0] = 1;
  byte i = 1;
  while(i < 16) {
    mL[i] = (mL[i-1]+random(-1,2))%4;
    mR[i] = (mR[i-1]+random(-1,2))%4;
    if(mL[i] + mR[i] < 5) i++; //important to have all step on map achievable
  }
  lcd.setCursor(1,0);
  for(byte i = 1; i < 16; i++) 
    lcd.write(borderLeft[mL[i]]);

  lcd.setCursor(1,1);
  for(byte i = 1; i < 16; i++) 
    lcd.write(borderRight[mR[i]]);

}

boolean isHit() {
    if(
      (playerPos <= 8 && mL[0] == 3) ||
      (playerPos <= 6 && mL[0] == 2) ||
      (playerPos <= 4 && mL[0] == 1) ||
      (playerPos <= 2 && mL[0] == 0) ||
      (playerPos >= 7 && mR[0] == 3) ||
      (playerPos >= 9 && mR[0] == 2) ||
      (playerPos >= 11 && mR[0] == 1) ||
      (playerPos >= 13 && mR[0] == 0)
      )
      return true;
      
  return false;
}
int btnLeftTimestamp = 0;
int btnRightTimestamp = 0;
void movePlayerLeft() {
  if(millis() - btnLeftTimestamp > 200 && playerPos > 1) {
    playerPos--;
    btnLeftTimestamp = millis();
  }
}

void movePlayerRight() {
  if(millis() - btnRightTimestamp > 200 && playerPos < 14) {
    playerPos++;
    btnRightTimestamp = millis();
  }
}
