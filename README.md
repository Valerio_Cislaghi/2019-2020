# Sistemi Embedded 2019-2020

**Corso per laurea triennale**

A.A. 2019/2020 (SECONDO SEMESTRE)

  * [Programma ufficiale](https://www.unimi.it/it/corsi/insegnamenti-dei-corsi-di-laurea/2020/sistemi-embedded)
  * [Gruppo Telegram](https://t.me/joinchat/ALnoPw8wxMVMet7DYU1NZQ) (per coordinamento e messaggi urgenti)

## Orari e aule:

* Mercoledì, 14.30-16.30, aula Omega
* Venerdì, 10.30-12.30, aula Delta

(lezioni senza intervallo "inline", ma si comincia circa 10 min dopo orario ufficiale e si termina circa 10 min prima)

**NB: per ora lezioni in "virtuale" su Zoom (https://zoom.us/j/486100406)**

[Archivio video (registrato via Zoom)](https://unimi2013-my.sharepoint.com/:f:/g/personal/andrea_trentini_unimi_it/EksODX8opO5GnIyFeELJ7oMBiGZGUZCL4316V87prW4YmQ?e=gHPIn4)

[Archivio video alternativo (registrato da "mario per il popolo")](https://drive.google.com/drive/folders/1dsHOLWqg60wPaovjKeJ7vXyfUtegpAm_)

## Modalità esame:

* orale/scritto (domande aperte)
* progetto da presentare in aula



## AVVISI

* INIZIO CORSO: venerdì 13/3/2020 (ore 10:30) in "aula virtuale" (**CAUSA COVID-19**), tecnologia ancora da decidere, probabilmente piattaforma del CTU, aggiungersi al gruppo Telegram per coordinamento

* Gentile richiesta, che vale per tutte le pagine di questo repo: se notate cose da correggere fatelo pure, idem vale per l'elenco delle cose fatte (diario), aggiungete pure se mi sono dimenticato degli item, grazie!


## Libro di testo

Alexjan Carraturo, Andrea Trentini

SISTEMI EMBEDDED: TEORIA E PRATICA (seconda edizione) 

[[ledizioni](http://www.ledizioni.it/prodotto/a-carraturo-a-trentini-sistemi-embedded-teoria-pratica/)] (anche PDF scaricabile gratuitamente)

[[amazon](https://www.amazon.it/Sistemi-embedded-pratica-Alexjan-Carraturo/dp/8867059432/)]


## Livello di conoscenza/approfondimento richiesto per ogni capitolo

Nota bene: per ora (feb 2020) questo è il livello di approfondimento richiesto rispetto agli argomenti trattati nel testo, ma la situazione potrebbe variare (e ne discuteremo in aula) in funzione di cosa riusciremo a coprire a lezione e agli interventi esterni che riusciremo ad organizzare.

1. Intro: tutto, in dettaglio
1. Concetti: tutto, in dettaglio
1. Richiami: tutto, in dettaglio
1. Architetture: tutto, in dettaglio
1. Mem: tutto, in dettaglio
1. S.O.: overview
1. Linux: overview
1. FreeRTOS: overview
1. Arduino: tutto, in dettaglio
1. Rete: tutto, in dettaglio

* App A: overview
* App B: overview (anche perché questi argomenti saranno oggetto di lab)

P.S. man mano che scrivo (e pubblico) le domande d'esame mi rendo conto del livello di approfondimento che intendo per i vari capitoli



## Esami

Il calendario (sul SIFA) è sempre da considerarsi indicativo perché le sessioni d'esame si fanno su appuntamento da concordare col docente (via mail).

Quindi importante iscriversi al SIFA per avere poi la possibilità di verbalizzare, ma ancora più importante comunicare col docente le proprie intenzioni e lo stato dei lavori in modo da pianificare per tempo le sessioni.

Inoltre è FONDAMENTALE pubblicare con un certo anticipo il proprio repository di progetto sulla pagina "progetti" per dare modo al docente di valutare codice, architettura, etc.



## Comunicazioni col docente

Preferibilmente via mail (andrea punto trentini lumachella unimi punto it), importante mettere un subject "parlante" (es. "sistemi embedded, domanda su costante RC") in modo da identificare subito il contesto.

Non abbiate paura di scrivere, meglio una mail in più di una in meno, specie se per concordare l'esame (sia il progetto che la data effettiva di presentazione).



## Hardware consigliato

* ESP32
* ESP8266
* Arduino "classico"

## URL delle board aggiuntive

Da mettere nel "board manager" dell'IDE (File -> Preferences -> Additional Boards Manager)

```ssh
http://digistump.com/package_digistump_index.json
http://arduino.esp8266.com/stable/package_esp8266com_index.json
http://raw.githubusercontent.com/esp8266/Arduino/master/boards.txt
http://adafruit.github.io/arduino-board-index/package_adafruit_index.json
https://raw.githubusercontent.com/sparkfun/Arduino_Boards/master/IDE_Board_Manager/package_sparkfun_index.json
https://dl.espressif.com/dl/package_esp32_index.json
```

## Documentazione varia

FIXME inserire link doc ufficiale ESP32 => arduino

- Datasheet Atmel-42735-8-bit-AVR-Microcontroller-ATmega328/P [[download github](https://github.com/b1gtuna/rainbowduino/raw/master/Atmel-42735-8-bit-AVR-Microcontroller-ATmega328-328P_datasheet.pdf)]
- Utilizzo del VIN PIN come output/utilizzo di power supply esterni [[stackexchange]](https://arduino.stackexchange.com/a/51878)

### Libro su 'git' (IMPORTANTE)

https://git-scm.com/book/en/v2



## Scratchpad (temporaneo, condiviso)

https://demo.codimd.org/oRIG4LK1SsaDsvm7FtCXpA

Una volta usavo un editor collaborativo chiamato [Gobby](https://gobby.github.io/) ma è sempre stato un po' macchinoso perché usa porte di rete non standard e vengono *firewallate* da UniMi...



## Tirocini

Contattare docente per successivo colloquio in azienda

### ST

#### Embedded firmware (ref. Mapelli)

(6 mesi retribuiti)

* processore MP1 triple core SoC, dual core A7(OpenSTLinux) + un M4 (STM32Cube + Rtos) in configurazione AMP collegati tramite OpenAMP https://www.st.com/en/microcontrollers-microprocessors/stm32mp1-series.html
* le attivita coinvolgono sia il lato Linux (Python3 e C mixed code, in ambiente OpenSTLinux) https://www.st.com/en/embedded-software/stm32-mpu-openstlinux-distribution.html sia, lato M4, C Fw in ambiente STM32Cube e Rtos

	Per la posizione FW su M4 possiamo proporre il porting e lo sviluppo di un driver per  stepper motor controller   e di un driver per sensore di prossimita basato su tecnologia ToF, entrambi serviranno allo sviluppo di una applicazione Real Time integrata con A7 Linux via OpenAMP protocol. https://www.st.com/en/imaging-and-photonics-solutions/proximity-sensors.html

	Il lavoro e l'ambiente di sviluppo sono piuttosto complessi data l'architettura di MP1, credo l'ideale sarebbe uno stage semestrale per una quinquennale, come al solito un po di esperienza pratica aiuterebbe, se hai un candidato lo colloquieremmo volentieri. Grazie, ciao

### Librerie Arduino (ref. Parata)

(incollo risposta)

Ciao Andrea,

Grazie per la proposta.

Io invece avrei da proporre una tesi sullo stesso filone di quella di Noah Rosa.
	
Quindi lo sviluppo di librerie Arduino per shield ST da pubblicare su https://github.com/stm32duino.
	
In particolare avrei da proporre il porting di shield di Motor Drivers, oppure di uno shield industriale basato su componenti MEMS, oppure di uno shield basato su sensori ToF.
	
Per me andrebbero bene anche studenti di triennale che hanno bisogno di un tirocinio di 3 mesi.
	
Nel caso la candidata fosse interessata, vorrei poterle fare un colloquio conoscitivo via Skype o Teams.
	
Aggiungo anche che a causa dell'emergenza Covid19, in questo periodo stiamo lavorando quasi esclusivamente da casa in smart working e questa modalita' andra' avanti per un tempo abbastanza lungo.
	
Quindi e' molto probabile che anche il tirocinio si svolgera' prevalentemente in modalita' smart working.

Ciao e a presto,

Carlo
